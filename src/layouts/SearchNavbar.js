import React, { useEffect, useRef } from "react";
import { useHistory } from "react-router-dom";
import { ReactComponent as PasteIcon } from "../static/images/clipIcons/paste.svg";
import { ReactComponent as SearchIcon } from "../static/images/clipIcons/search.svg";

import cryptoBankLogo from "../static/images/logos/atmsLogo.svg";
import cryptoBankLogoWhite from "../static/images/logos/atmsLogo.svg";
import banker from "../static/images/clipIcons/banker.svg";
import banks from "../static/images/clipIcons/banks.svg";
import backIcon from "../static/images/clipIcons/back.svg";

function SearchNavbar({ invert, type, setType, search, setSearch, onClick }) {
  const history = useHistory();
  const ref = useRef();
  useEffect(() => {
    ref.current && ref.current.focus();
  }, []);
  return (
    <nav className="searchNavbar">
      <img
        src={invert ? cryptoBankLogoWhite : cryptoBankLogo}
        alt="cryptobanklogo"
        className="crypto-img"
        onClick={() => {
          try {
            onClick();
          } catch (error) {}
          history.push("/");
        }}
      />
      <img
        src={backIcon}
        alt=""
        className="backLogo"
        onClick={() => {
          try {
            onClick();
          } catch (error) {}
          history.push("/");
        }}
      />
      <label className="navSearch">
        <input
          ref={ref}
          type="text"
          className="textInput"
          placeholder={`Find A ${type}`}
          value={search}
          onChange={(e) => setSearch(e.target.value)}
        />
        <PasteIcon
          className="clipIcon"
          onClick={() => {
            navigator.clipboard.readText().then((text) => setSearch(text));
          }}
        />
        <SearchIcon className="searchIcn" />
        <div
          className={`icon ${type === "bank"}`}
          onClick={() => setType("bank")}
        >
          <img src={banks} alt="" />
        </div>
        <div
          className={`icon ${type === "banker"}`}
          onClick={() => setType("banker")}
        >
          <img src={banker} alt="" />
        </div>
      </label>
    </nav>
  );
}

export default SearchNavbar;
