import React, { useState, useContext } from "react";
import BankerAddAccount from "./BankerAddAccount/BankerAddAccount";
import bankerBlue from "../../static/images/logos/bankerBlue.svg";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit, faPlus, faTrash } from "@fortawesome/free-solid-svg-icons";
import { DetailPageContext } from "../../Contexts/DetailPageContext";

function BankerManageComponent() {
  const { setShowInstitutes } = useContext(DetailPageContext);
  const [step, setStep] = useState("");
  function getContent() {
    switch (step) {
      case "add":
        return <BankerAddAccount onClose={() => setStep("")} />;
      default:
        return (
          <div className="bankerManageComponent">
            <img src={bankerBlue} alt="" className="bankerLogo" />
            <p className="message">
              You Have Successfully Logged Into Your Banker Profile.
              <br /> What Would You Like To Do Today?
            </p>
            <div
              className="btnOptions"
              onClick={() => {
                setStep("add");
                setShowInstitutes(true);
              }}
            >
              <FontAwesomeIcon icon={faPlus} /> Add New Account
            </div>
            <div className="btnOptions inv">
              <FontAwesomeIcon icon={faEdit} /> Edit Existing Account
            </div>
            <div className="btnOptions">
              <FontAwesomeIcon icon={faTrash} /> Delete Existing Account
            </div>
          </div>
        );
    }
  }
  return <>{getContent()}</>;
}

export default BankerManageComponent;
