import { faCaretDown, faCaretUp } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useContext, useState } from "react";
import Scrollbars from "react-custom-scrollbars";
import { DetailPageContext } from "../../../Contexts/DetailPageContext";

function BankerDetailSide() {
  const {
    instSelected,
    nickName,
    currency,
    branchId,
    accountId,
    minAmt,
    maxAmt,
    benefEmail,
    benefAddress,
  } = useContext(DetailPageContext);
  const [openDetail, setOpenDetail] = useState("");
  return (
    <div className="bankerDetailSide">
      <img src={instSelected?.profile_image} alt="" className="bankLogo" />
      <div className="bankName">{instSelected?.institute_name}</div>
      <div className="bankCountry">{instSelected?.country_name}</div>
      <Scrollbars className="detailScroll">
        <div className="detaiAccordin">
          <div
            className="title"
            onClick={() => {
              if (openDetail === "genaralInfo") {
                setOpenDetail("");
              } else {
                setOpenDetail("genaralInfo");
              }
            }}
          >
            General Information
            <FontAwesomeIcon
              icon={openDetail === "genaralInfo" ? faCaretUp : faCaretDown}
            />
          </div>
          {openDetail === "genaralInfo" ? (
            <>
              <div className="detailItm">
                <div className="label">Insitution ID</div>
                <div className="value">{instSelected.bank_id}</div>
              </div>
              <div className="detailItm">
                <div className="label">Address</div>
                <div className="value">
                  <span>
                    {instSelected?.locations[0].building_name},<br />
                    {instSelected?.locations[0].city},<br />
                    {instSelected?.locations[0].country_name},<br />
                  </span>
                </div>
              </div>
              <div className="detailItm">
                <div className="label">Name</div>
                <div className="value">{nickName}</div>
              </div>
              <div className="detailItm">
                <div className="label">Swift</div>
                <div className="value"></div>
              </div>
            </>
          ) : (
            ""
          )}
        </div>
        <div className="detaiAccordin">
          <div
            className="title"
            onClick={() => {
              if (openDetail === "accountInfo") {
                setOpenDetail("");
              } else {
                setOpenDetail("accountInfo");
              }
            }}
          >
            Account Info
            <FontAwesomeIcon icon={faCaretDown} />
          </div>
          {openDetail === "accountInfo" ? (
            <>
              <div className="detailItm">
                <div className="label">Currency</div>
                <div className="value">{currency}</div>
              </div>
              <div className="detailItm">
                <div className="label">Account #</div>
                <div className="value">{accountId}</div>
              </div>
              <div className="detailItm">
                <div className="label">Transit #</div>
                <div className="value">{branchId}</div>
              </div>
              <div className="detailItm">
                <div className="label">Branch Address</div>
                <div className="value"></div>
              </div>
            </>
          ) : (
            ""
          )}
        </div>
        <div className="detaiAccordin">
          <div
            className="title"
            onClick={() => {
              if (openDetail === "beneficiaryInfo") {
                setOpenDetail("");
              } else {
                setOpenDetail("beneficiaryInfo");
              }
            }}
          >
            Beneficiary Info
            <FontAwesomeIcon icon={faCaretDown} />
          </div>
          {openDetail === "beneficiaryInfo" ? (
            <>
              <div className="detailItm">
                <div className="label">Address</div>
                <div className="value">{benefAddress}</div>
              </div>
              <div className="detailItm">
                <div className="label">Email</div>
                <div className="value">{benefEmail}</div>
              </div>
              <div className="detailItm">
                <div className="label">Minimum</div>
                <div className="value">{minAmt}</div>
              </div>
              <div className="detailItm">
                <div className="label">Maximum</div>
                <div className="value">{maxAmt}</div>
              </div>
            </>
          ) : (
            ""
          )}
        </div>
      </Scrollbars>
    </div>
  );
}

export default BankerDetailSide;
