import Axios from "axios";
import React, { Fragment, useContext, useState } from "react";
import { DetailPageContext } from "../../../Contexts/DetailPageContext";
import { MainContext } from "../../../Contexts/MainContext";

function BankerAddAccount({ onClose }) {
  const { email, toastShowOn } = useContext(MainContext);
  const {
    instSelected,
    setShowInstitutes,
    bankerAddStep,
    setBankerAddStep,
    nickName,
    setNickName,
    currency,
    setCurrency,
    branchId,
    setBranchId,
    accountId,
    setAccountId,
    minAmt,
    setMinAmt,
    maxAmt,
    setMaxAmt,
    benefEmail,
    setBenefEmail,
    benefAddress,
    setbenefAddress,
  } = useContext(DetailPageContext);

  const addAccount = () => {
    Axios.post(
      "https://accountingtool.apimachine.com/attach_account_to_institution",
      {
        nick_name: nickName,
        institution_id: instSelected.bank_id,
        email_id: email,
        branch_id: branchId,
        account_id: accountId,
        currency: currency,
        minimum_transfer_amount: parseFloat(minAmt),
        maximum_transfer_amount: parseFloat(maxAmt),
        additional_data: {
          email: benefEmail,
          address: benefAddress,
        },
      }
    ).then(({ data }) => {
      if (data.status) {
        toastShowOn("Account Created");
        try {
          onClose();
        } catch (error) {}
      } else {
        toastShowOn("Something Went Wrong");
      }
    });
  };
  const steps = [
    <Fragment key={bankerAddStep}>
      <div className="title">Step {bankerAddStep + 1}: Adding New Account</div>
      <div className="subTitle">
        Which Financial Institution Is Your Account With?
      </div>
      <div
        className="btnNext"
        onClick={() => {
          setBankerAddStep(bankerAddStep + 1);
          setShowInstitutes(false);
        }}
      >
        Next Step
      </div>
    </Fragment>,
    <Fragment key={bankerAddStep}>
      <div className="title">Step {bankerAddStep + 1}: Adding New Account</div>
      <div className="subTitle">
        What Name Do You Want To Display For This Account?
      </div>
      <input
        type="text"
        className="inpBox"
        placeholder="Ex. Cookies Delight"
        value={nickName}
        onChange={(e) => setNickName(e.target.value)}
      />
      <div
        className="btnNext"
        onClick={() => setBankerAddStep(bankerAddStep + 1)}
      >
        Next Step
      </div>
    </Fragment>,
    <Fragment key={bankerAddStep}>
      <div className="title">Step {bankerAddStep + 1}: Adding New Account</div>
      <div className="subTitle">
        This Institution Currenctly Supports These Currencies. Select One Of The
        Currencies To See Who Can Support.
      </div>
      <div className="currencies">
        {instSelected?.all_supported_currency?.map((cur) => (
          <div
            key={cur.currency_code}
            className="option"
            onClick={() => {
              setBankerAddStep(bankerAddStep + 1);
              setCurrency(cur.currency_code);
            }}
          >
            {cur.full_name}
          </div>
        ))}
      </div>
    </Fragment>,
    <Fragment key={bankerAddStep}>
      <div className="title">Step {bankerAddStep + 1}: Adding New Account</div>
      <div className="subTitle">
        What Is Your {instSelected?.branch_id_name}?
      </div>
      <input
        type="text"
        className="inpBox"
        placeholder="EX, 131461"
        value={branchId}
        onChange={(e) => setBranchId(e.target.value)}
      />
      <div
        className="btnNext"
        onClick={() => setBankerAddStep(bankerAddStep + 1)}
      >
        Next Step
      </div>
    </Fragment>,
    <Fragment key={bankerAddStep}>
      <div className="title">Step {bankerAddStep + 1}: Adding New Account</div>
      <div className="subTitle">
        What is your {instSelected?.account_id_name}?
      </div>
      <input
        type="text"
        className="inpBox"
        placeholder="EX, 131461"
        value={accountId}
        onChange={(e) => setAccountId(e.target.value)}
      />
      <div
        className="btnNext"
        onClick={() => setBankerAddStep(bankerAddStep + 1)}
      >
        Next Step
      </div>
    </Fragment>,
    <Fragment key={bankerAddStep}>
      <div className="title">Step {bankerAddStep + 1}: Adding New Account</div>
      <div className="subTitle">
        What Is The Minimum Amount They Can Send At Once
      </div>
      <input
        type="text"
        className="inpBox"
        placeholder="EX, 100"
        value={minAmt}
        onChange={(e) => setMinAmt(e.target.value)}
      />
      <div
        className="btnNext"
        onClick={() => setBankerAddStep(bankerAddStep + 1)}
      >
        Next Step
      </div>
    </Fragment>,
    <Fragment key={bankerAddStep}>
      <div className="title">Step {bankerAddStep + 1}: Adding New Account</div>
      <div className="subTitle">
        What Is The Maximum Amount They Can Send At Once
      </div>
      <input
        type="text"
        className="inpBox"
        placeholder="EX, 13100000"
        value={maxAmt}
        onChange={(e) => setMaxAmt(e.target.value)}
      />
      <div
        className="btnNext"
        onClick={() => setBankerAddStep(bankerAddStep + 1)}
      >
        Next Step
      </div>
    </Fragment>,
    <Fragment key={bankerAddStep}>
      <div className="title">Step {bankerAddStep + 1}: Adding New Account</div>
      <div className="subTitle">What Is The Email For This Account?</div>
      <input
        type="email"
        className="inpBox"
        placeholder="example@example.com"
        value={benefEmail}
        onChange={(e) => setBenefEmail(e.target.value)}
      />
      <div
        className="btnNext"
        onClick={() => setBankerAddStep(bankerAddStep + 1)}
      >
        Next Step
      </div>
    </Fragment>,
    <Fragment key={bankerAddStep}>
      <div className="title">Step {bankerAddStep + 1}: Adding New Account</div>
      <div className="subTitle">What Is The Beneficiaries Addreess?</div>
      <input
        type="text"
        className="inpBox"
        placeholder="Enter Address"
        value={benefAddress}
        onChange={(e) => setbenefAddress(e.target.value)}
      />
      <div className="btnNext" onClick={addAccount}>
        Publish
      </div>
    </Fragment>,
  ];
  return <div className="bankerAddAccount">{steps[bankerAddStep]}</div>;
}

export default BankerAddAccount;
