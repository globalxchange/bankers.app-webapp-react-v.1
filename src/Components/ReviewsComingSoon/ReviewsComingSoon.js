import React, { useRef } from "react";
import OnOutsideClick from "../../utils/OnOutsideClick";

function ReviewsComingSoon({ onClose, logo }) {
  const ref = useRef();
  OnOutsideClick(ref, () => {
    try {
      onClose();
    } catch (error) {}
  });
  return (
    <div className="tellersComingSoonWrapper reviewComingSoon">
      <div className="tellersComingSoon" ref={ref}>
        <img src={logo} alt="" className="logo" />
        <div className="comingSoon">Reviews Coming Soon</div>
        <div
          className="btClose"
          onClick={() => {
            try {
              onClose();
            } catch (error) {}
          }}
        >
          Close
        </div>
      </div>
    </div>
  );
}

export default ReviewsComingSoon;
