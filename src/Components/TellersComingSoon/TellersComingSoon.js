import React, { useRef } from "react";
import OnOutsideClick from "../../utils/OnOutsideClick";
import tellersLogo from "../../static/images/logos/tellersLogo.svg";

function TellersComingSoon({ onClose }) {
  const ref = useRef();
  OnOutsideClick(ref, () => {
    try {
      onClose();
    } catch (error) {}
  });
  return (
    <div className="tellersComingSoonWrapper">
      <div className="tellersComingSoon" ref={ref}>
        <img src={tellersLogo} alt="" className="logo" />
        <div className="comingSoon">Coming Soon</div>
        <div
          className="btClose"
          onClick={() => {
            try {
              onClose();
            } catch (error) {}
          }}
        >
          Close
        </div>
      </div>
    </div>
  );
}

export default TellersComingSoon;
