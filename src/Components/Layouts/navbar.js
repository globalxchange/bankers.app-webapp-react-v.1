import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import "./layout.scss";
import cryptoBankLogo from "../../static/images/logos/atmsLogo.svg";
import cryptoBankLogoWhite from "../../static/images/logos/atmsLogo.svg";

function Navbar({ invert, className }) {
  const history = useHistory();
  const [navOpen, setNavOpen] = useState(false);

  return (
    <nav className={`navbarMain ${className} ${invert}`}>
      <div
        onClick={() => setNavOpen(!navOpen)}
        className={
          "hamburger hamburger--squeeze" + (navOpen ? " is-active" : "")
        }
      >
        <span className="hamburger-box m-auto">
          <span className="hamburger-inner"></span>
        </span>
      </div>
      {navOpen && (
        <div className="navlist">
          <div className="menuItm" onClick={() => history.push("/apps")}>
            Apps
          </div>
          <div className="menuItm" onClick={() => history.push("/searchTxn")}>
            Tracking
          </div>
        </div>
      )}
      <div className="navBtn" onClick={() => history.push("/apps")}>
        Apps
      </div>
      <img
        src={invert ? cryptoBankLogoWhite : cryptoBankLogo}
        alt="cryptobanklogo"
        className="crypto-img"
        onClick={() => history.push("/")}
      />
      <div className="navBtn" onClick={() => history.push("/searchTxn")}>
        Tracking
      </div>
    </nav>
  );
}

export default Navbar;
