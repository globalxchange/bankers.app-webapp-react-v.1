import React from "react";

function LoginProgressBar({ progress = 0, message, position = "fixed" }) {
  return (
    <div className="loginPogressWrapper" style={{ position }}>
      <div className="progress">
        <div className="progress-bar-login" style={{ width: `${progress}%` }} />
      </div>
      <div className="messge">{message}</div>
    </div>
  );
}

export default LoginProgressBar;
