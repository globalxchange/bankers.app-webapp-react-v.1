import {
  faEye,
  faEyeSlash,
  faSpinner,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Axios from "axios";
import React, { useContext, useEffect, useState } from "react";

import bankerBlue from "../../static/images/logos/bankerBlue.svg";
import { MainContext } from "../../Contexts/MainContext";
import InvalidBanker from "./InvalidBanker";
import BankerManageComponent from "../BankerManageComponent/BankerManageComponent";
import LoginProgressBar from "../LoginProgressBar/LoginProgressBar";

function BankerManageLoginComponent({ banker }) {
  const { login, toastShowOn, email } = useContext(MainContext);
  const [progress, setProgress] = useState(0);
  const [inValid, setInValid] = useState(false);
  const [loading, setLoading] = useState(false);
  const [emailid, setEmailid] = useState("");
  const [password, setPassword] = useState("");
  const [showPassword, setShowPassword] = useState("");

  useEffect(() => {
    login();
  }, []);

  const config = {
    onUploadProgress: function (progressEvent) {
      setProgress(
        Math.round((progressEvent.loaded * 100) / progressEvent.total)
      );
    },
  };

  const loginValidate = (e) => {
    e.preventDefault();
    Axios.get(
      `https://teller2.apimachine.com/admin/allBankers?email=${emailid}`
    ).then(({ data }) => {
      if (data.status) {
        if (!Boolean(data?.data?.length)) {
          setInValid(true);
        } else {
          loginUser();
          setInValid(false);
        }
      } else {
        setInValid(true);
      }
    });
  };

  const loginUser = () => {
    if (/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(emailid)) {
      setLoading(true);
      Axios.post(
        "https://gxauth.apimachine.com/gx/user/login",
        {
          email: emailid,
          password,
        },
        config
      )
        .then((response) => {
          const { data } = response;
          if (data.status) {
            login(emailid, data.accessToken, data.idToken);
          } else {
            toastShowOn(data.message);
          }
        })
        .catch((error) => {
          toastShowOn(error.message ? error.message : "Some Thing Went Wrong!");
        })
        .finally(() => {
          setLoading(false);
        });
    } else {
      toastShowOn("Enter Valid EmailId");
    }
  };

  return (
    <div className="bankerLogin">
      {email ? (
        <BankerManageComponent banker={banker} />
      ) : inValid ? (
        <InvalidBanker setInValid={setInValid} />
      ) : (
        <div className="loginWrapper">
          <div className="logoWrap">
            <img src={bankerBlue} alt="" className="logo" />
          </div>
          <form className="login-form mx-5" onSubmit={loginValidate}>
            <div className="group">
              <input
                type="text"
                name="email"
                value={emailid}
                onChange={(e) => setEmailid(e.target.value)}
                required="required"
              />
              <span className="highlight" />
              <span className="bar" />
              <label>Email</label>
            </div>
            <div className="group">
              <input
                type={showPassword ? "text" : "password"}
                name="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                required="required"
              />
              <span className="highlight" />
              <span className="bar" />
              <FontAwesomeIcon
                className="eye"
                onClick={() => {
                  setShowPassword(!showPassword);
                }}
                icon={showPassword ? faEyeSlash : faEye}
              />
              <label>Password</label>
            </div>
            <div className="group">
              <button type="submit" disabled={loading}>
                {loading ? <FontAwesomeIcon icon={faSpinner} spin /> : "Login"}
              </button>
            </div>
          </form>
          {loading && (
            <LoginProgressBar progress={progress} position="absolute" />
          )}
        </div>
      )}
    </div>
  );
}

export default BankerManageLoginComponent;
