import React from "react";
import bankerBlue from "../../static/images/logos/bankerBlue.svg";

function InvalidBanker({ setInValid }) {
  return (
    <div className="invalidBanker">
      <div className="logoWrap">
        <img src={bankerBlue} alt="" />
      </div>
      <p className="notBanker">
        Sorry But You Are Not A Banker. Please Login With A Registered Banker
        Account Or Register To Become A Banker.
      </p>
      <div className="buttons">
        <div className="btnRegister">Register</div>
        <div className="btnTryAgain" onClick={() => setInValid(false)}>
          Try Again
        </div>
      </div>
    </div>
  );
}

export default InvalidBanker;
