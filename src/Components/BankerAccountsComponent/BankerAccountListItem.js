import Axios from "axios";
import React, { useEffect, useState } from "react";
import Skeleton from "react-loading-skeleton";
import { useHistory } from "react-router-dom";

function BankerAccountListItem({ account }) {
  const history = useHistory();
  const [instituteDetail, setInstituteDetail] = useState("");
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    setLoading(true);
    Axios.get(
      `https://accountingtool.apimachine.com/get-single-institute/${account?.institution_id}`
    )
      .then(({ data }) => {
        if (data.status) setInstituteDetail(data.data);
      })
      .finally(() => setLoading(false));
  }, [account.institution_id]);
  return (
    <div
      className="account"
      onClick={() => history.push(`/${account.institution_id}/${account._id}`)}
    >
      {loading ? (
        <>
          <Skeleton className="img" />
          <div className="text">
            <Skeleton className="name" width={350} />
            <Skeleton className="country" width={200} />
          </div>
        </>
      ) : (
        <>
          <img src={instituteDetail.profile_image} alt="" className="img" />
          <div className="text">
            <div className="name">{account.nick_name}</div>
            <div className="country">{instituteDetail.country_name}</div>
          </div>
        </>
      )}
    </div>
  );
}

export default BankerAccountListItem;
