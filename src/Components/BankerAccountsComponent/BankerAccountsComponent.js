import Axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import Scrollbars from "react-custom-scrollbars";
import Skeleton from "react-loading-skeleton";
import { useLocation } from "react-router-dom";
import { DetailPageContext } from "../../Contexts/DetailPageContext";
import BankerAccountListItem from "./BankerAccountListItem";

function BankerAccountsComponent() {
  const { pathname } = useLocation();
  const { institutionSelected, baknerSelected } = useContext(DetailPageContext);
  const [accountList, setAccountList] = useState([]);
  const [loading, setLoading] = useState(true);
  const [count, setCount] = useState(0);
  useEffect(() => {
    setLoading(true);
    Axios.get(
      pathname.includes("/banker/")
        ? `https://accountingtool.apimachine.com/list_accounts_per_user?email_id=${baknerSelected.email}`
        : `https://accountingtool.apimachine.com/list_accounts_per_institution?institution_id=${institutionSelected._id}`
    )
      .then(({ data }) => {
        if (data.status) {
          setAccountList(data.data);
          setCount(data.no_of_user);
        } else {
          setAccountList([]);
          setCount(0);
        }
      })
      .finally(() => setLoading(false));
  }, [institutionSelected, baknerSelected, pathname]);
  return (
    <div className="bankerAccountsComponent">
      <div className="title">
        Verified Accounts <span>{count}</span>
      </div>
      <div className="subTitle">
        Click On The Account Which Concerns Your Transaction
      </div>
      <Scrollbars
        className="accountListScrl"
        renderThumbHorizontal={() => <div />}
      >
        {loading
          ? Array(5)
              .fill("")
              .map(() => (
                <div className="account">
                  <Skeleton className="img" />
                  <div className="text">
                    <Skeleton className="name" width={350} />
                    <Skeleton className="country" width={200} />
                  </div>
                </div>
              ))
          : accountList.map((account) => (
              <BankerAccountListItem key={account._id} account={account} />
            ))}
      </Scrollbars>
    </div>
  );
}

export default BankerAccountsComponent;
