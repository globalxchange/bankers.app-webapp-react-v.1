import React, { useContext, useEffect, useState } from "react";

import { DetailPageContext } from "../../Contexts/DetailPageContext";
import redirectIcon from "../../static/images/clipIcons/redirect.svg";
import backIcon from "../../static/images/clipIcons/back.svg";
import Skeleton from "react-loading-skeleton";
import { useHistory } from "react-router-dom";
import Scrollbars from "react-custom-scrollbars";
import Axios from "axios";

function InstitutionProfile({ loading }) {
  const { institutionSelected, setInstitutionSelected } = useContext(
    DetailPageContext
  );
  useEffect(() => {
    Axios.get(
      `https://accountingtool.apimachine.com/get-single-institute/${institutionSelected?._id}`
    ).then(({ data }) => {
      if (data.status) {
        setInstitutionSelected(data.data);
      }
    });
  }, [institutionSelected._id, setInstitutionSelected]);
  const history = useHistory();

  const [view, setView] = useState("");
  function getContent() {
    switch (view) {
      case "General Information":
        return (
          <div className="detailTable">
            <div className="headTitle">General Information</div>
            <div className="detailContent">
              <div className="item">
                <span>Institution Type</span>
                <span className="cap">
                  {institutionSelected?.institute_type}
                </span>
              </div>
              <div className="item">
                <span>Website</span>
                <span>{institutionSelected?.Website}</span>
              </div>
              <div className="item">
                <span>Abbreviation</span>
                <span className="cap">{institutionSelected?.short_name}</span>
              </div>
              <div className="item">
                <span>Phone Number </span>
                <span>{institutionSelected?.phone_number}</span>
              </div>
              <div className="item">
                <span>Headquarters</span>
                <span className="cap">{institutionSelected?.country_name}</span>
              </div>
              <div className="item">
                <span>SWIFT Code</span>
                <span>{institutionSelected?.swift_code}</span>
              </div>
            </div>
          </div>
        );
      case "Products":
      case "Branches":
      case "Links":
      case "Terminology":
        break;
      default:
        return (
          <>
            <div className="detaiAccordin">
              {loading ? (
                <Skeleton className="title" />
              ) : (
                <div
                  className="title"
                  onClick={() => setView("General Information")}
                >
                  General Information
                </div>
              )}
            </div>
            <div className="detaiAccordin">
              {loading ? (
                <Skeleton className="title" />
              ) : (
                <div className="title" onClick={() => setView("Products")}>
                  Products
                </div>
              )}
            </div>
            <div className="detaiAccordin">
              {loading ? (
                <Skeleton className="title" />
              ) : (
                <div className="title" onClick={() => setView("Branches")}>
                  Branches
                  <span>{institutionSelected?.no_of_branches}</span>{" "}
                </div>
              )}
            </div>
            <div className="detaiAccordin">
              {loading ? (
                <Skeleton className="title" />
              ) : (
                <div className="title" onClick={() => setView("Branches")}>
                  Currencies
                  <span>{institutionSelected?.no_of_currencies}</span>{" "}
                </div>
              )}
            </div>
            <div className="detaiAccordin">
              {loading ? (
                <Skeleton className="title" />
              ) : (
                <div className="title" onClick={() => setView("Links")}>
                  Links
                </div>
              )}
            </div>
            <div className="detaiAccordin">
              {loading ? (
                <Skeleton className="title" />
              ) : (
                <div className="title" onClick={() => {}}>
                  Terminology
                </div>
              )}
            </div>
          </>
        );
    }
  }

  return (
    <div
      className="institutionProfile"
      style={{
        backgroundImage: `url('${institutionSelected?.cover_image}')`,
      }}
    >
      <div className="logoRow">
        {loading ? (
          <>
            <Skeleton className="boxIcon" />
            <Skeleton className="bankLogo" />
            <Skeleton className="boxIcon" />
          </>
        ) : (
          <>
            <div
              className="boxIcon"
              onClick={() => {
                if (view) setView("");
                else history.push(`/bank/`);
              }}
            >
              <img src={backIcon} alt="" className="icon" />
            </div>
            <img
              src={institutionSelected?.profile_image}
              alt=""
              className="bankLogo"
            />
            <div className="boxIcon">
              <img src={redirectIcon} alt="" className="icon" />
            </div>
          </>
        )}
      </div>
      {loading ? (
        <Skeleton className="bankName" width={400} />
      ) : (
        <div className="bankName">{institutionSelected?.institute_name}</div>
      )}
      {loading ? (
        <Skeleton className="bankCountry" width={350} />
      ) : (
        <div className="bankCountry">{institutionSelected?.country_name}</div>
      )}
      <Scrollbars
        className="detailScroll"
        renderThumbHorizontal={() => <div />}
        renderThumbVertical={() => <div />}
        renderTrackHorizontal={() => <div />}
        renderTrackVertical={() => <div />}
      >
        {getContent()}
      </Scrollbars>
    </div>
  );
}

export default InstitutionProfile;
