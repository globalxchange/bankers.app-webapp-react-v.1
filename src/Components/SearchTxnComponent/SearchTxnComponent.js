import React, { useContext, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import MobileSearchComponent from "../../Components/MobileSearchComponent/MobileSearchComponent";
import { MainContext } from "../../Contexts/MainContext";
import pasteIcon from "../../static/images/clipIcons/paste.svg";
import searchIcon from "../../static/images/clipIcons/search.svg";

function SearchTxnComponent({ cbw, setCbw }) {
  const history = useHistory();
  const { toastShowOn } = useContext(MainContext);
  const [search, setSearch] = useState("");
  const [searchFocus, setSearchFocus] = useState(false);
  return (
    <div className={`serchArea ${cbw && "hide"}`}>
      <div className="title">Lets Get Started</div>
      <div className="subTitle">Enter A CryptoBankWire Code</div>
      <label className="inpBox">
        <input
          type="text"
          className="searchInp"
          placeholder="Ex. 125DJAGB99"
          value={search}
          onChange={(e) => setSearch(e.target.value)}
          onFocus={() => setSearchFocus(true)}
          // onBlur={() => setSearchFocus(false)}
        />
        <img
          src={pasteIcon}
          alt=""
          className="paste"
          onClick={() => {
            navigator.clipboard
              .readText()
              .then((clipText) => setSearch(clipText));
          }}
        />
        <img
          src={searchIcon}
          alt=""
          onClick={() => {
            if (search) history.push(`/account/${search}`);
            else toastShowOn("Enter A CryptoBankWire Code To Proceed");
          }}
        />
      </label>
      <div
        className="btnSearch"
        onClick={() => {
          if (search) history.push(`/account/${search}`);
          else toastShowOn("Enter A CryptoBankWire Code To Proceed");
        }}
      >
        Search
      </div>
      <Link to="/" className="goBack">
        Go Back Home
      </Link>
      <div className="mobileFooter" onClick={() => setCbw(true)}>
        Don’t Have A CBW Code?
      </div>
      {searchFocus && (
        <MobileSearchComponent
          search={search}
          setSearch={setSearch}
          setSearchFocus={setSearchFocus}
        />
      )}
    </div>
  );
}

export default SearchTxnComponent;
