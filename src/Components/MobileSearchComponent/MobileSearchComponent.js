import React from "react";
import pasteIcon from "../../static/images/clipIcons/paste.svg";
import searchIcon from "../../static/images/clipIcons/search.svg";
import backIcon from "../../static/images/clipIcons/back.svg";

function MobileSearchComponent({ search, setSearch, setSearchFocus }) {
  return (
    <div className="mobileSerch">
      <label className="inpBox">
        <img
          src={backIcon}
          alt=""
          className="back"
          onClick={() => setSearchFocus(false)}
        />
        <input
          type="text"
          className="searchInp"
          placeholder="Ex. TD Canada Trust"
          value={search}
          onChange={(e) => setSearch(e.target.value)}
        />
        <img
          src={pasteIcon}
          alt=""
          className="paste"
          onClick={() => {
            navigator.clipboard
              .readText()
              .then((clipText) => setSearch(clipText));
          }}
        />
        <img className="search" src={searchIcon} alt="" />
      </label>
    </div>
  );
}

export default MobileSearchComponent;
