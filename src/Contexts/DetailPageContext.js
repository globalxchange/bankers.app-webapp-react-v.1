import React, { createContext, useState } from "react";

export const DetailPageContext = createContext();

function DetailPageContextProvider({ children }) {
  const [institutionSelected, setInstitutionSelected] = useState("");
  const [baknerSelected, setBankerSelected] = useState("");
  const [instSelected, setInstSelected] = useState({});
  const [showInstitutes, setShowInstitutes] = useState(false);

  //Banker Add
  const [bankerAddStep, setBankerAddStep] = useState(0);
  const [nickName, setNickName] = useState("");
  const [currency, setCurrency] = useState("");
  const [branchId, setBranchId] = useState("");
  const [accountId, setAccountId] = useState("");
  const [minAmt, setMinAmt] = useState("");
  const [maxAmt, setMaxAmt] = useState("");
  const [benefEmail, setBenefEmail] = useState("");
  const [benefAddress, setbenefAddress] = useState("");

  return (
    <DetailPageContext.Provider
      value={{
        institutionSelected,
        setInstitutionSelected,
        baknerSelected,
        setBankerSelected,

        instSelected,
        setInstSelected,
        showInstitutes,
        setShowInstitutes,
        bankerAddStep,
        setBankerAddStep,
        nickName,
        setNickName,
        currency,
        setCurrency,
        branchId,
        setBranchId,
        accountId,
        setAccountId,
        minAmt,
        setMinAmt,
        maxAmt,
        setMaxAmt,
        benefEmail,
        setBenefEmail,
        benefAddress,
        setbenefAddress,
      }}
    >
      {children}
    </DetailPageContext.Provider>
  );
}
export default DetailPageContextProvider;
