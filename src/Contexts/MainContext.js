import React, { createContext, useEffect, useState } from "react";
import Axios from "axios";
import Toast from "../Components/Toast/Toast";

export const MainContext = createContext();

function MainContextProvider({ children }) {
  const [email, setEmail] = useState(
    localStorage.getItem("LoginAccount") || ""
  );
  const [accessToken, setAccessToken] = useState(
    localStorage.getItem("AccessToken") || ""
  );
  const [token, setToken] = useState(localStorage.getItem("Token") || "");

  useEffect(() => {
    localStorage.setItem("LoginAccount", email);
  }, [email]);
  useEffect(() => {
    localStorage.setItem("AccessToken", accessToken);
  }, [accessToken]);
  useEffect(() => {
    localStorage.setItem("Token", token);
  }, [token]);

  const login = (paramEmail, paramAccessToken, paramToken) => {
    setEmail(paramEmail);
    setAccessToken(paramAccessToken);
    setToken(paramToken);
  };

  const [profilePic, setProfilePic] = useState("");
  const [profileName, setProfileName] = useState("");

  useEffect(() => {
    if (email && token) {
      Axios.post("https://comms.globalxchange.com/coin/verifyToken", {
        email,
        token: token,
      }).then((res) => (res.data.status ? "" : login("", "", "")));

      Axios.get(
        `https://comms.globalxchange.com/user/details/get?email=${email}`
      ).then((res) => {
        const { data } = res;
        if (data.status) {
          setProfileName(data.user.name);
          setProfilePic(data.user.profile_img);
        }
      });
    }
  }, [email, token]);

  const [toastShow, setToastShow] = useState(false);
  const [toastMessage, setToastMessage] = useState("");
  const toastShowOn = (message) => {
    setToastShow(true);
    setToastMessage(message);
    setTimeout(() => {
      setToastShow(false);
    }, 3000);
  };

  const [appSelected, setAppSelected] = useState("");
  useEffect(() => {
    !email && setAppSelected("");
  }, [email]);

  const [modalSessionExpOpen, setModalSessionExpOpen] = useState(false);
  const validateToken = async (paramEmail, paramToken) => {
    const res = await Axios.post(
      "https://comms.globalxchange.com/coin/verifyToken",
      {
        email: paramEmail,
        token: paramToken,
      }
    );
    if (res.data && res.data.status) {
      return true;
    } else {
      setModalSessionExpOpen(true);
      return false;
    }
  };

  return (
    <MainContext.Provider
      value={{
        login,
        email,
        token,
        profilePic,
        profileName,
        modalSessionExpOpen, //To Delete
        toastShowOn,
        appSelected,
        setAppSelected,
        validateToken,
      }}
    >
      {children}
      <Toast show={toastShow} message={toastMessage} />
    </MainContext.Provider>
  );
}
export default MainContextProvider;
