import React from "react";
import { Route, Switch } from "react-router-dom";
import AccountDetailPage from "./pages/AccountDetailPage/AccountDetailPage";
import AppsPage from "./pages/AppsPage";
import HomePage from "./pages/HomePage";
import InstitutionDetailPage from "./pages/InstitutionDetailPage/InstitutionDetailPage";
import InstitutionsPage from "./pages/InstitutionsPage/InstitutionsPage";
import SerchTxnPage from "./pages/SerchTxnPage/SerchTxnPage";

function Routes() {
  return (
    <Switch>
      <Route exact path="/" component={HomePage} />
      <Route exact path="/apps" component={AppsPage} />
      <Route exact path="/searchTxn" component={SerchTxnPage} />
      <Route exact path="/institution" component={InstitutionsPage} />
      <Route exact path="/exchanges" component={InstitutionsPage} />
      <Route exact path="/banker/:bankerId" component={InstitutionDetailPage} />
      <Route exact path="/bank/:instId" component={InstitutionDetailPage} />
      <Route exact path="/banker" component={InstitutionDetailPage} />
      <Route exact path="/bank" component={InstitutionDetailPage} />
      <Route exact path="/account/:id" component={AccountDetailPage} />
      <Route exact path="/:instId/:id" component={AccountDetailPage} />
    </Switch>
  );
}

export default Routes;
