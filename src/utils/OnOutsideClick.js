import { useEffect } from 'react';

function OnOutsideClick(ref, callBack, view) {
  useEffect(() => {
    function handleClickOutside(event) {
      if (view) {
        if (
          ref.current &&
          !ref.current.contains(event.target) &&
          view.contains(event.target)
        ) {
          callBack();
        }
      } else if (ref.current && !ref.current.contains(event.target)) {
        callBack();
      }
    }

    // Bind the event listener
    document.addEventListener('mousedown', handleClickOutside);
    return () => {
      // Unbind the event listener on clean up
      document.removeEventListener('mousedown', handleClickOutside);
    };
    // eslint-disable-next-line
  }, [ref]);
}

export default OnOutsideClick;
