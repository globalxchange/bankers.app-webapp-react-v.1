import React from "react";
import { BrowserRouter } from "react-router-dom";
import "@teamforce/fullpage-search/dist/index.css";

import DetailPageContextProvider from "./Contexts/DetailPageContext";
import MainContextProvider from "./Contexts/MainContext";
import SearchContextProvider from "./Contexts/SearchContext";
import Routes from "./Routes";
import useWindowDimensions from "./utils/WindowSize";

function App() {
  const { height } = useWindowDimensions();
  return (
    <div className="appWrap" style={{ height }}>
      <MainContextProvider>
        <BrowserRouter>
          <DetailPageContextProvider>
            <SearchContextProvider>
              <Routes />
            </SearchContextProvider>
          </DetailPageContextProvider>
        </BrowserRouter>
      </MainContextProvider>
    </div>
  );
}

export default App;
