import React, { useEffect, useContext, useState } from "react";
import Skeleton from "react-loading-skeleton";
import { useHistory } from "react-router-dom";
import Scrollbars from "react-custom-scrollbars";
import Axios from "axios";

import { DetailPageContext } from "../../Contexts/DetailPageContext";
import SearchNavbar from "../../layouts/SearchNavbar";

function InstitutionsPage({ search, setSearch, type, setType, goBack }) {
  const history = useHistory();
  const { setInstitutionSelected, setBankerSelected } = useContext(
    DetailPageContext
  );
  const [instituteList, setInstituteList] = useState([]);
  const [exchangeList, setExchangeList] = useState([]);
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    setLoading(true);
    if (type === "bank") {
      Axios.get("https://accountingtool.apimachine.com/getlist-of-institutes")
        .then(({ data }) => {
          if (data.status) {
            setInstituteList(data.data);
          }
        })
        .finally(() => setLoading(false));
    } else {
      Axios.get("https://teller2.apimachine.com/admin/allBankers")
        .then(({ data }) => {
          if (data.status) {
            setExchangeList(data.data);
          }
        })
        .finally(() => setLoading(false));
    }
  }, [type]);
  return (
    <>
      <SearchNavbar
        search={search}
        setSearch={setSearch}
        type={type}
        setType={setType}
        onClick={() => {
          goBack();
        }}
      />
      <div className="institutionPage">
        <Scrollbars className="scrollList" renderThumbVertical={() => <div />}>
          {loading
            ? Array(6)
                .fill("")
                .map((a, i) => (
                  <div className="bankItem" key={i}>
                    <Skeleton className="logo" />
                    <Skeleton className="bankName" width={420} />
                    <Skeleton className="country" />
                  </div>
                ))
            : type === "bank"
            ? instituteList
                .filter(
                  (institute) =>
                    institute?.institute_name
                      ?.toLowerCase()
                      .includes(search?.toLowerCase()) ||
                    institute?.short_name
                      ?.toLowerCase()
                      .includes(search?.toLowerCase())
                )
                .map((institute) => (
                  <div
                    className="bankItem"
                    key={institute._id}
                    onClick={() => {
                      setInstitutionSelected(institute);
                      history.push(`/bank/${institute._id}`);
                    }}
                  >
                    <img
                      className="logo"
                      src={institute.profile_image}
                      alt=""
                    />
                    <div className="text">
                      <span className="bankName">
                        {institute.institute_name}
                      </span>
                      <div className="country">{institute.country_name}</div>
                    </div>
                  </div>
                ))
            : exchangeList
                .filter((banker) =>
                  banker?.bankerTag
                    ?.toLowerCase()
                    .includes(search?.toLowerCase())
                )
                .map((banker) => (
                  <div
                    className="bankItem"
                    key={banker._id}
                    onClick={() => {
                      setBankerSelected(banker);
                      history.push(`/banker/${banker._id}`);
                    }}
                  >
                    <img className="logo" src={banker.profilePicURL} alt="" />
                    <div className="text">
                      <span className="bankName">{banker.bankerTag}</span>
                      <div className="country">{banker.country}</div>
                    </div>
                  </div>
                ))}
        </Scrollbars>
      </div>
    </>
  );
}

export default InstitutionsPage;
