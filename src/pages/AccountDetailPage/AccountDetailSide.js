import React, { useEffect, useState } from "react";
import { faCaretDown, faCaretUp } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Axios from "axios";
import Scrollbars from "react-custom-scrollbars";
import Skeleton from "react-loading-skeleton";
import { Link, useHistory, useParams } from "react-router-dom";

import redirectIcon from "../../static/images/clipIcons/redirect.svg";
import backIcon from "../../static/images/clipIcons/back.svg";

function AccountDetailSide({ accountDetail, invalid, loading, bankerDetail }) {
  const history = useHistory();
  const { instId } = useParams();
  const [instDetail, setInstDetail] = useState({});
  const [openDetail, setOpenDetail] = useState("");
  const [instLoading, setInstLoading] = useState(true);
  useEffect(() => {
    setInstLoading(true);
    Axios.get(
      `https://accountingtool.apimachine.com/get-single-institute/${accountDetail?.institution_id}`
    )
      .then(({ data }) => {
        setInstDetail(data.data);
      })
      .finally(() => setInstLoading(false));
  }, [accountDetail]);
  return (
    <div className={`AccountDetailSide ${invalid}`}>
      {invalid ? (
        <>
          <div className="bankName">That Is Not A Valid CBW Code</div>
          <div className="bankCountry">
            Please Try Entering Another Code Or Search Through Either Of These
            Lists
          </div>
          <div className="optButtons">
            <Link to="/institution" className="optBtn">
              Institutions
            </Link>
            <Link to="/exchanges" className="optBtn">
              Exchanges
            </Link>
          </div>
        </>
      ) : (
        <>
          <div className="logoRow">
            {loading || instLoading ? (
              <Skeleton className="bankLogo" />
            ) : (
              <>
                <div
                  className="boxIcon"
                  onClick={() => history.push(`/bank/${instId}`)}
                >
                  <img src={backIcon} alt="" className="icon" />
                </div>
                <img
                  src={instDetail?.profile_image}
                  alt=""
                  className="bankLogo"
                />
                <div className="boxIcon">
                  <img src={redirectIcon} alt="" className="icon" />
                </div>
              </>
            )}
          </div>
          {loading || instLoading ? (
            <Skeleton className="bankName" width={400} />
          ) : (
            <div className="bankName">{instDetail?.institute_name}</div>
          )}
          {loading || instLoading ? (
            <Skeleton className="bankCountry" width={350} />
          ) : (
            <div className="bankCountry">{bankerDetail?.bankerTag}</div>
          )}
          <Scrollbars
            className="detailScroll"
            renderThumbHorizontal={() => <div />}
            renderThumbVertical={() => <div />}
          >
            <div className="detaiAccordin">
              {loading || instLoading ? (
                <Skeleton className="title" />
              ) : (
                <div
                  className="title"
                  onClick={() => {
                    if (openDetail === "genaralInfo") {
                      setOpenDetail("");
                    } else {
                      setOpenDetail("genaralInfo");
                    }
                  }}
                >
                  General Information
                  <FontAwesomeIcon
                    icon={
                      openDetail === "genaralInfo" ? faCaretUp : faCaretDown
                    }
                  />
                </div>
              )}
              {openDetail === "genaralInfo" ? (
                <>
                  <div className="detailItm">
                    <div className="label">Insitution ID</div>
                    <div className="value">{instDetail.bank_id}</div>
                  </div>
                  <div className="detailItm">
                    <div className="label">Address</div>
                    <div className="value">
                      <span>
                        {instDetail?.locations &&
                          instDetail?.locations[0]?.building_name}
                        ,<br />
                        {instDetail?.locations &&
                          instDetail?.locations[0]?.city}
                        ,<br />
                        {instDetail?.locations &&
                          instDetail?.locations[0]?.country_name}
                        ,<br />
                      </span>
                    </div>
                  </div>
                  <div className="detailItm">
                    <div className="label">Name</div>
                    <div className="value">{accountDetail?.nick_name}</div>
                  </div>
                  <div className="detailItm">
                    <div className="label">Swift</div>
                    <div className="value">{accountDetail?.swift_code}</div>
                  </div>
                </>
              ) : (
                ""
              )}
            </div>
            <div className="detaiAccordin">
              {loading || instLoading ? (
                <Skeleton className="title" />
              ) : (
                <div
                  className="title"
                  onClick={() => {
                    if (openDetail === "accountInfo") {
                      setOpenDetail("");
                    } else {
                      setOpenDetail("accountInfo");
                    }
                  }}
                >
                  Account Info
                  <FontAwesomeIcon icon={faCaretDown} />
                </div>
              )}
              {openDetail === "accountInfo" ? (
                <>
                  <div className="detailItm">
                    <div className="label">Currency</div>
                    <div className="value">{accountDetail?.currency}</div>
                  </div>
                  <div className="detailItm">
                    <div className="label">Account #</div>
                    <div className="value">{accountDetail?.account_id}</div>
                  </div>
                  <div className="detailItm">
                    <div className="label">Transit #</div>
                    <div className="value">{accountDetail?.branch_id}</div>
                  </div>
                  <div className="detailItm">
                    <div className="label">Branch Address</div>
                    <div className="value"></div>
                  </div>
                </>
              ) : (
                ""
              )}
            </div>
            <div className="detaiAccordin">
              {loading || instLoading ? (
                <Skeleton className="title" />
              ) : (
                <div
                  className="title"
                  onClick={() => {
                    if (openDetail === "beneficiaryInfo") {
                      setOpenDetail("");
                    } else {
                      setOpenDetail("beneficiaryInfo");
                    }
                  }}
                >
                  Beneficiary Info
                  <FontAwesomeIcon icon={faCaretDown} />
                </div>
              )}
              {openDetail === "beneficiaryInfo" ? (
                <>
                  <div className="detailItm">
                    <div className="label">Address</div>
                    <div className="value">
                      {accountDetail?.additional_data?.address}
                    </div>
                  </div>
                  <div className="detailItm">
                    <div className="label">Email</div>
                    <div className="value">
                      {accountDetail?.additional_data?.email}
                    </div>
                  </div>
                  <div className="detailItm">
                    <div className="label">Minimum</div>
                    <div className="value">
                      {accountDetail?.minimum_transfer_amount}
                    </div>
                  </div>
                  <div className="detailItm">
                    <div className="label">Maximum</div>
                    <div className="value">
                      {accountDetail?.maximum_transfer_amount}
                    </div>
                  </div>
                </>
              ) : (
                ""
              )}
            </div>
          </Scrollbars>
        </>
      )}
    </div>
  );
}

export default AccountDetailSide;
