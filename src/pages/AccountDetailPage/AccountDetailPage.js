import Axios from "axios";
import React, { useState, useEffect, useRef } from "react";
import Skeleton from "react-loading-skeleton";
import { useHistory, useParams } from "react-router-dom";

import SearchTxnComponent from "../../Components/SearchTxnComponent/SearchTxnComponent";
import SearchNavbar from "../../layouts/SearchNavbar";
import AccountDetailSide from "./AccountDetailSide";

function AccountDetailPage() {
  const { id } = useParams();
  const history = useHistory();
  const [instituteList, setInstituteList] = useState([]);
  const [institutionSelected, setInstitutionSelected] = useState("");
  const [index, setIndex] = useState();
  const [loading, setLoading] = useState(false);
  // const next = () => {
  //   if (index === instituteList.length - 1) {
  //     setIndex(0);
  //   } else {
  //     setIndex(index + 1);
  //   }
  // };
  // const prev = () => {
  //   if (index === 0) {
  //     setIndex(instituteList.length - 1);
  //   } else {
  //     setIndex(index - 1);
  //   }
  // };

  useEffect(() => {
    setLoading(true);
    Axios.get("https://accountingtool.apimachine.com/getlist-of-institutes")
      .then(({ data }) => {
        if (data.status) {
          setInstituteList(data.data);
        }
      })
      .finally(() => setLoading(false));
  }, []);

  useEffect(() => {
    if (institutionSelected && instituteList) {
      instituteList.forEach((institute, i) => {
        if (institute._id === institutionSelected._id) {
          setIndex(i);
        }
      });
    } else {
      setIndex(0);
    }
  }, [instituteList, institutionSelected]);
  useEffect(() => {
    if (instituteList) {
      setInstitutionSelected(instituteList[index]);
    }
  }, [index, instituteList, setInstitutionSelected]);

  const isPrev = (i, list) => {
    if (index === 0) {
      if (i === list.length - 1) return true;
    } else {
      if (i === index - 1) return true;
    }
    return false;
  };
  const isPrevPrev = (i, list) => {
    if (index === 0) {
      if (i === list.length - 2) return true;
    }
    if (index === 1) {
      if (i === list.length - 1) return true;
    } else {
      if (i === index - 2) return true;
    }
    return false;
  };
  const isNext = (i, list) => {
    if (index === list.length - 1) {
      if (i === 0) return true;
    } else {
      if (i === index + 1) return true;
    }
    return false;
  };
  const isNextNext = (i, list) => {
    if (index === list.length - 1) {
      if (i === 1) return true;
    } else if (index === list.length - 2) {
      if (i === 0) return true;
    } else {
      if (i === index + 2) return true;
    }
    return false;
  };

  const [detailLoading, setDetailLoading] = useState(true);
  const [invalid, setInvalid] = useState(false);
  const [accountDetail, setAccountDetail] = useState({});
  useEffect(() => {
    setDetailLoading(true);
    Axios.get(
      `https://accountingtool.apimachine.com/get_account_detail_to_institution?account_id=${id}`
    )
      .then(({ data }) => {
        if (data.status) {
          setAccountDetail(data.data[0]);
          if (instituteList) {
            instituteList.forEach((institute, i) => {
              if (institute._id === data.data[0]?.institution_id) {
                setIndex(i);
              }
            });
          }
        } else {
          setInvalid(true);
        }
      })
      .finally(() => setDetailLoading(false));
  }, [id, instituteList]);

  const [bankerDetail, setBankerDetail] = useState();
  useEffect(() => {
    accountDetail &&
      Axios.get(
        `https://teller2.apimachine.com/admin/allBankers?email=${accountDetail?.email_id}`
      ).then(({ data }) => {
        if (data.status && data.data.length) {
          setBankerDetail(data.data[0]);
        } else {
          setBankerDetail({ bankerTag: accountDetail?.email_id });
        }
      });
  }, [accountDetail]);

  const ref = useRef();
  useEffect(() => {
    ref.current.style.setProperty(
      "--main-color",
      (accountDetail?.color_code && accountDetail?.color_code[0]) || "#18191d"
    );
    return () => {};
  }, [accountDetail]);

  return (
    <>
      <SearchNavbar
        type="bank"
        setType={(type) => {
          history.push(`/${type}`);
        }}
        setSearch={() => history.push("/bank")}
      />
      <div className="AccountDetail" ref={ref}>
        {invalid ? (
          <SearchTxnComponent />
        ) : (
          <div className="carouselWrapper">
            <div className="carouselVert">
              {loading ? (
                <>
                  <div className="bankItem active">
                    <Skeleton className="logo" />
                    <Skeleton className="bankName" width={400} />
                    <Skeleton className="country" />
                  </div>
                  <div className="bankItem next">
                    <Skeleton className="logo" />
                    <Skeleton className="bankName" width={400} />
                    <Skeleton className="country" />
                  </div>
                  <div className="bankItem nextnext">
                    <Skeleton className="logo" />
                    <Skeleton className="bankName" width={400} />
                    <Skeleton className="country" />
                  </div>
                  <div className="bankItem prevprev">
                    <Skeleton className="logo" />
                    <Skeleton className="bankName" width={400} />
                    <Skeleton className="country" />
                  </div>
                  <div className="bankItem prev">
                    <Skeleton className="logo" />
                    <Skeleton className="bankName" width={400} />
                    <Skeleton className="country" />
                  </div>
                </>
              ) : (
                instituteList.map((institute, i) => {
                  return (
                    <div
                      className={`bankItem ${
                        index === i
                          ? "active"
                          : isPrev(i, instituteList)
                          ? "prev"
                          : isNext(i, instituteList)
                          ? "next"
                          : isPrevPrev(i, instituteList)
                          ? "prevprev"
                          : isNextNext(i, instituteList)
                          ? "nextnext"
                          : "inactive"
                      }`}
                      onClick={() => {
                        // setInstitutionSelected(institute);
                      }}
                      key={institute._id}
                    >
                      <img
                        className="logo"
                        src={institute.profile_image}
                        alt=""
                      />
                      <span className="bankName">
                        {institute.institute_name}
                      </span>
                      <div className="country">{institute.country_name}</div>
                    </div>
                  );
                })
              )}
            </div>
          </div>
        )}
        <AccountDetailSide
          bankerDetail={bankerDetail}
          accountDetail={accountDetail}
          invalid={invalid}
          loading={detailLoading}
        />
      </div>
    </>
  );
}

export default AccountDetailPage;
