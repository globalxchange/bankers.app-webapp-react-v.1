import React, { useContext, useEffect, useRef, useState } from "react";
import { useHistory } from "react-router-dom";
import Scrollbars from "react-custom-scrollbars";
import Axios from "axios";
import { Fragment } from "react";
import { CategoryModal, SearchBarSingle } from "@teamforce/fullpage-search";
import Skeleton from "react-loading-skeleton";

import useWindowDimensions from "../../utils/WindowSize.js";

import Navbar from "../../Components/Layouts/navbar";
import banker from "../../static/images/clipIcons/banker.svg";
import banks from "../../static/images/clipIcons/banks.svg";
import InstitutionsPage from "../InstitutionsPage/InstitutionsPage.js";
import { SearchContext } from "../../Contexts/SearchContext.js";

function HomePage() {
  const { width } = useWindowDimensions();
  const history = useHistory();
  const [searchOn, setSearchOn] = useState(false);
  const { search, setSearch } = useContext(SearchContext);
  const [instituteList, setInstituteList] = useState([]);
  const [exchangeList, setExchangeList] = useState([]);
  const [intermediariesList, setIntermediariesList] = useState([]);
  const [tellersList, setTellersList] = useState([]);
  const [tellersLoading, setTellersLoading] = useState(false);
  const [exchangeLoading, setExchangeLoading] = useState(false);
  const [instituteLoading, setInstituteLoading] = useState(false);
  const [itermLoading, setItermLoading] = useState(false);

  const [tab, setTab] = useState("Construction");
  const [tabIndex, setTabIndex] = useState(0);
  const [categoryModal, setCategoryModal] = useState(false);
  const tabs = useRef([
    {
      img: banks,
      label: "Banks",
      onClick: () => {
        setTab("Banks");
        setTabIndex(0);
      },
    },
    {
      img: banker,
      label: "Bankers",
      onClick: () => {
        setTab("Bankers");
        setTabIndex(1);
      },
    },
    {
      img: "",
      label: "Tellers",
      onClick: () => {
        setTab("Tellers");
        setTabIndex(2);
      },
    },
    {
      img: "",
      label: "Intermediaries",
      onClick: () => {
        setTab("Intermediaries");
        setTabIndex(3);
      },
    },
  ]);
  useEffect(() => {
    setTab(tabs.current[tabIndex]?.label);
  }, [tabIndex]);

  useEffect(() => {
    setExchangeLoading(true);
    setInstituteLoading(true);
    setItermLoading(true);
    setTellersLoading(true);
    Axios.get("https://accountingtool.apimachine.com/getlist-of-institutes")
      .then(({ data }) => {
        if (data.status) {
          setInstituteList(data.data);
        }
      })
      .finally(() => setInstituteLoading(false));
    Axios.get("https://teller2.apimachine.com/admin/allBankers")
      .then(({ data }) => {
        if (data.status) {
          setExchangeList(data.data);
        }
      })
      .finally(() => setExchangeLoading(false));
    Axios.get(
      "https://comms.globalxchange.com/coin/vault/service/payment/methods/get"
    )
      .then(({ data }) => {
        if (data.status) {
          setIntermediariesList(data.methods);
        }
      })
      .finally(() => setItermLoading(false));
    Axios.get("https://teller2.apimachine.com/admin/users")
      .then(({ data }) => {
        if (data.status) {
          setTellersList(data.data);
        }
      })
      .finally(() => setTellersLoading(false));
  }, []);

  useEffect(() => {
    if (search) {
      setSearchOn(true);
    }
  }, [search]);
  function getContent() {
    switch (tab) {
      case "Banks":
        return (
          <Fragment key="bank">
            {instituteLoading
              ? Array(20)
                  .fill("")
                  .map((_, i) => (
                    <div className="winner" key={i}>
                      <Skeleton className="icon" circle />
                    </div>
                  ))
              : instituteList.map((bank) => (
                  <div
                    className="winner new"
                    onClick={() => history.push(`/bank/${bank._id}`)}
                    key={bank._id}
                  >
                    <img src={bank.profile_image} alt="" className="icon" />
                    <div className="name">{bank.institute_name}</div>
                  </div>
                ))}
          </Fragment>
        );
      case "Bankers":
        return (
          <Fragment key="banker">
            {exchangeLoading
              ? Array(20)
                  .fill("")
                  .map((_, i) => (
                    <div className="winner" key={i}>
                      <Skeleton className="icon" circle />
                    </div>
                  ))
              : exchangeList.map((banker) => (
                  <div
                    className="winner new"
                    key={banker._id}
                    onClick={() => history.push(`/banker/${banker._id}`)}
                  >
                    <img src={banker.profilePicURL} className="icon" alt="" />
                    <div className="name">{banker.bankerTag}</div>
                  </div>
                ))}
          </Fragment>
        );
      case "Intermediaries":
        return (
          <Fragment key="banker">
            {itermLoading
              ? Array(20)
                  .fill("")
                  .map((_, i) => (
                    <div className="winner" key={i}>
                      <Skeleton className="icon" circle />
                    </div>
                  ))
              : intermediariesList.map((method) => (
                  <div
                    className="winner new"
                    key={method._id}
                    // onClick={() => history.push(`/banker/${banker._id}`)}
                  >
                    <img src={method.icon} className="icon" alt="" />
                    <div className="name">{method.name}</div>
                  </div>
                ))}
          </Fragment>
        );
      case "Tellers":
        return (
          <Fragment key="tellers">
            {tellersLoading
              ? Array(20)
                  .fill("")
                  .map((_, i) => (
                    <div className="winner" key={i}>
                      <Skeleton className="icon" circle />
                    </div>
                  ))
              : tellersList.map((teller) => (
                  <div className="winner new" key={teller._id}>
                    <img src={teller.profilePicURL} className="icon" alt="" />
                    <div className="name">{`${teller.firstName} ${teller.lastName}`}</div>
                  </div>
                ))}
          </Fragment>
        );

      default:
        break;
    }
  }

  const [type, setType] = useState("bank");
  const ref = useRef();
  useEffect(() => {
    !search && ref.current && ref.current.focus();
  }, [search]);

  useEffect(() => {
    if (type === "bank") {
      setTab("Banks");
    } else if (type === "banker") {
      setTab("Bankers");
    }
  }, [type]);

  return (
    <>
      {searchOn ? (
        <InstitutionsPage
          search={search}
          setSearch={setSearch}
          type={type}
          setType={setType}
          goBack={() => setSearchOn(false)}
        />
      ) : (
        <div className="landingPage">
          <Navbar />
          <div className="banker">
            <p className="main-title">Your Financial Passport</p>
            <p className="sub-title">
              {width > 756
                ? "The ATM’s App Allows Users To Navigate Between Financial Systems"
                : "Navigate Through The Changing World Of Finance"}
            </p>
            <SearchBarSingle
              search={search}
              setSearch={setSearch}
              placeholder={`Find ${tab}`}
              iconOne={{
                active: true,
                icon: tabs.current[tabIndex].img,
                name: tab,
                onClick: () => {
                  setCategoryModal(true);
                },
              }}
            />
          </div>
          <div className="titles">
            {tabs.current.map((inv, i) => (
              <span
                key={inv.label}
                className={`user ${tab === inv.label}`}
                onClick={() => {
                  setTab(inv.label);
                  setTabIndex(i);
                }}
              >
                {inv.label}
              </span>
            ))}
          </div>
          {tabs.current.length && (
            <div className="titlesScroll">
              <span
                className="btns"
                onClick={() => setTabIndex((i) => (i - 1 === -1 ? 3 : i - 1))}
              >
                {tabs.current[tabIndex - 1 === -1 ? 3 : tabIndex - 1].label}
              </span>
              <span className="selected">{tabs.current[tabIndex].label}</span>
              <span
                className="btns"
                onClick={() => setTabIndex((i) => (i + 1 === 4 ? 0 : i + 1))}
              >
                {tabs.current[tabIndex + 1 === 4 ? 0 : tabIndex + 1].label}
              </span>
            </div>
          )}
          <div className="footer-winners-wrapper">
            <Scrollbars
              renderTrackHorizontal={(props) => <div {...props}></div>}
              renderThumbHorizontal={(props) => <div></div>}
              renderView={(props) => (
                <div {...props} className="footer-winners" />
              )}
            >
              {getContent()}
            </Scrollbars>
          </div>
          {categoryModal && (
            <CategoryModal
              categories={tabs.current}
              onClose={() => setCategoryModal(false)}
            />
          )}
        </div>
      )}
    </>
  );
}

export default HomePage;
