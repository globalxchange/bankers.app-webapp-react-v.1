import Axios from "axios";
import React, { useEffect, useState } from "react";
import Skeleton from "react-loading-skeleton";
import Navbar from "../../Components/Layouts/navbar";
import atmsIcon from "../../static/images/clipIcons/liquidApps/atms.svg";
import bankerIcon from "../../static/images/clipIcons/liquidApps/banker.svg";
import blockCheckIcon from "../../static/images/clipIcons/liquidApps/blockCheck.svg";
import instaCryptoIcon from "../../static/images/clipIcons/liquidApps/instaCrypto.svg";
import tellersIcon from "../../static/images/clipIcons/liquidApps/tellers.svg";
import vaultsIcon from "../../static/images/clipIcons/liquidApps/vaults.svg";

function AppsPage() {
  const [appSelected, setAppSelected] = useState();
  const [appDetail, setAppDetail] = useState();
  const [appDetailLoading, setAppDetailLoading] = useState(false);
  useEffect(() => {
    setAppDetailLoading(true);
    appSelected &&
      Axios.get(
        `https://comms.globalxchange.com/gxb/apps/get?app_code=${appSelected}`
      )
        .then(({ data }) => {
          setAppDetail(data?.apps && data.apps[0]);
        })
        .finally(() => setAppDetailLoading(false));
  }, [appSelected]);
  return (
    <div className="appsPage">
      <Navbar />
      <div className="liquidity">
        <div className="title">
          {appSelected
            ? "Liquidity Apps"
            : "Click On One Of Our Liquidity Apps"}
        </div>
        <div className="appsList">
          <div
            className={`appWrap ${appSelected === "bankerapp"} ${
              !appSelected ? "noApp" : ""
            }`}
            onClick={() => setAppSelected("bankerapp")}
          >
            <img src={bankerIcon} alt="" className="app" />
            <div className="appLabel">Banker</div>
          </div>
          <div
            className={`appWrap ${appSelected === "lx"} ${
              !appSelected ? "noApp" : ""
            }`}
            onClick={() => setAppSelected("lx")}
          >
            <img src={atmsIcon} alt="" className="app" />
            <div className="appLabel">ATM’s</div>
          </div>
          <div
            className={`appWrap ${appSelected === "teller"} ${
              !appSelected ? "noApp" : ""
            }`}
            onClick={() => setAppSelected("teller")}
          >
            <img src={tellersIcon} alt="" className="app" />
            <div className="appLabel">Tellers</div>
          </div>
          <div
            className={`appWrap ${appSelected === ""} ${
              !appSelected ? "noApp" : ""
            }`}
            onClick={() => setAppSelected("")}
          >
            <img src={vaultsIcon} alt="" className="app" />
            <div className="appLabel">Vault</div>
          </div>
          <div
            className={`appWrap ${appSelected === "instacrypto"} ${
              !appSelected ? "noApp" : ""
            }`}
            onClick={() => setAppSelected("instacrypto")}
          >
            <img src={instaCryptoIcon} alt="" className="app" />
            <div className="appLabel">InstaCrypto</div>
          </div>
          <div
            className={`appWrap ${appSelected === "blockcheck"} ${
              !appSelected ? "noApp" : ""
            }`}
            onClick={() => setAppSelected("blockcheck")}
          >
            <img src={blockCheckIcon} alt="" className="app" />
            <div className="appLabel">BlockCheck</div>
          </div>
        </div>
      </div>
      {appSelected && (
        <div className="appDetailWrapper">
          <div className={`appDetail ${appSelected}`}>
            {appDetailLoading ? (
              <Skeleton className="title" />
            ) : (
              <div className="title">{appDetail?.app_name}</div>
            )}
            {appDetailLoading ? (
              <Skeleton className="desc" count={5} />
            ) : (
              <p className="desc">{appDetail?.long_description}</p>
            )}
            {appDetailLoading ? (
              <div className="btns">
                <Skeleton width={180} height={55} />
                <Skeleton width={180} height={55} />
              </div>
            ) : (
              <div className="btns">
                <a
                  href={
                    appDetail?.website?.includes("http")
                      ? appDetail?.website
                      : `https://${appDetail?.website}`
                  }
                  target="_blank"
                  rel="noopener noreferrer"
                  className="btnWeb"
                >
                  Web Page
                </a>
                <a
                  href={appDetail?.android_app_link}
                  target="_blank"
                  rel="noopener noreferrer"
                  className="btnMobile"
                >
                  Mobile
                </a>
              </div>
            )}
          </div>
        </div>
      )}
    </div>
  );
}

export default AppsPage;
