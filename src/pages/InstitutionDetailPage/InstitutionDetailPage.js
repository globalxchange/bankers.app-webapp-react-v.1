import Axios from "axios";
import React, { useState, useEffect, useContext, useRef } from "react";
import Skeleton from "react-loading-skeleton";
import { useHistory, useLocation, useParams } from "react-router-dom";
import { useSwipeable } from "react-swipeable";
import BankerAccountsComponent from "../../Components/BankerAccountsComponent/BankerAccountsComponent";
import BankerDetailSide from "../../Components/BankerManageComponent/BankerAddAccount/BankerDetailSide";

import BankerManageLoginComponent from "../../Components/BankerManageLoginComponent/BankerManageLoginComponent";
import InstitutionProfile from "../../Components/InstitutionProfile/InstitutionProfile";
import ReviewsComingSoon from "../../Components/ReviewsComingSoon/ReviewsComingSoon";
import { DetailPageContext } from "../../Contexts/DetailPageContext";
import { SearchContext } from "../../Contexts/SearchContext";
import SearchNavbar from "../../layouts/SearchNavbar";

function InstitutionDetailPage() {
  const history = useHistory();
  const { pathname } = useLocation();
  const { bankerId, instId } = useParams();
  const {
    institutionSelected,
    setInstitutionSelected,
    baknerSelected,
    setBankerSelected,
    showInstitutes,
    bankerAddStep,
  } = useContext(DetailPageContext);
  // To Set Color Code
  const ref = useRef();
  useEffect(() => {
    ref.current.style.setProperty(
      "--inst-page-color",
      institutionSelected?.color_code && institutionSelected.color_code[0]
        ? institutionSelected.color_code[0]
        : "#18191d"
    );
    return () => {};
  }, [institutionSelected]);

  const [instituteList, setInstituteList] = useState([]);
  const [exchangeList, setExchangeList] = useState([]);
  const [index, setIndex] = useState();
  const [loading, setLoading] = useState(false);
  const [tab, setTab] = useState("Profile");
  const [count, setCount] = useState(0);
  const { search, setSearch } = useContext(SearchContext);
  const next = () => {
    if (!pathname.includes("/banker") || showInstitutes) {
      if (index === instituteList.length - 1) {
        setIndex(0);
      } else {
        setIndex(index + 1);
      }
    } else {
      if (index === exchangeList.length - 1) {
        setIndex(0);
      } else {
        setIndex(index + 1);
      }
    }
  };
  const prev = () => {
    if (!pathname.includes("/banker") || showInstitutes) {
      if (index === 0) {
        setIndex(instituteList.length - 1);
      } else {
        setIndex(index - 1);
      }
    } else {
      if (index === 0) {
        setIndex(exchangeList.length - 1);
      } else {
        setIndex(index - 1);
      }
    }
  };
  const handlers = useSwipeable({
    onSwipedUp: () => next(),
    onSwipedDown: () => prev(),
    trackMouse: true,
  });

  useEffect(() => {
    setLoading(true);
    if (!pathname.includes("/banker") || showInstitutes) {
      Axios.get("https://accountingtool.apimachine.com/getlist-of-institutes")
        .then(({ data }) => {
          if (data.status) {
            setInstituteList(data.data);
            if (instId) {
              setInstitutionSelected(
                data.data.filter((inst) => inst._id === instId)[0]
              );
            } else setInstitutionSelected(data?.data[0]);
          }
        })
        .finally(() => setLoading(false));
    } else {
      Axios.get("https://teller2.apimachine.com/admin/allBankers")
        .then(({ data }) => {
          if (data.status) {
            setExchangeList(data.data);
            if (bankerId) {
              setBankerSelected(
                data.data.filter((banker) => banker._id === bankerId)[0]
              );
            } else setBankerSelected(data?.data[0]);
          }
        })
        .finally(() => setLoading(false));
    }
  }, [
    bankerId,
    instId,
    pathname,
    setBankerSelected,
    setInstitutionSelected,
    showInstitutes,
  ]);

  useEffect(() => {
    if (exchangeList && pathname.includes("/banker") && bankerId) {
      exchangeList.forEach((exchange, i) => {
        if (exchange._id === bankerId) {
          setIndex(i);
        }
      });
    } else if (pathname.includes("/bank") && instituteList && instId) {
      instituteList.forEach((institute, i) => {
        if (institute._id === instId) {
          setIndex(i);
        }
      });
    } else {
      setIndex(0);
    }
  }, [pathname, instituteList, exchangeList, showInstitutes, instId, bankerId]);
  useEffect(() => {
    if (index !== undefined) {
      if (pathname.includes("/banker") && exchangeList) {
        setBankerSelected(exchangeList[index]);
      } else if (pathname.includes("/bank") && instituteList) {
        setInstitutionSelected(instituteList[index]);
      }
    }
  }, [
    exchangeList,
    index,
    instituteList,
    pathname,
    setInstitutionSelected,
    setBankerSelected,
    showInstitutes,
  ]);

  const isPrev = (i, list) => {
    if (index === 0) {
      if (i === list.length - 1) return true;
    } else {
      if (i === index - 1) return true;
    }
    return false;
  };
  const isPrevPrev = (i, list) => {
    if (index === 0) {
      if (i === list.length - 2) return true;
    }
    if (index === 1) {
      if (i === list.length - 1) return true;
    } else {
      if (i === index - 2) return true;
    }
    return false;
  };
  const isNext = (i, list) => {
    if (index === list.length - 1) {
      if (i === 0) return true;
    } else {
      if (i === index + 1) return true;
    }
    return false;
  };
  const isNextNext = (i, list) => {
    if (index === list.length - 1) {
      if (i === 1) return true;
    } else if (index === list.length - 2) {
      if (i === 0) return true;
    } else {
      if (i === index + 2) return true;
    }
    return false;
  };

  function getTabContent() {
    switch (tab) {
      case "Manage":
        return <BankerManageLoginComponent banker={institutionSelected} />;
      case "ATM’s":
        return <BankerAccountsComponent />;
      case "Profile":
        return <InstitutionProfile loading={loading} />;
      case "Reviews":
        return (
          <ReviewsComingSoon
            logo={
              pathname.includes("/banker")
                ? baknerSelected?.profilePicURL
                : institutionSelected?.profile_image
            }
            onClose={() => setTab("Profile")}
          />
        );
      default:
        return (
          <div className="detailContent">
            <div className="detRow"></div>
            <div className="detRow"></div>
            <div className="detRow"></div>
          </div>
        );
    }
  }
  const [firstTry, setFirstTry] = useState(true);
  useEffect(() => {
    if (firstTry) {
      setFirstTry(false);
    } else {
      if (exchangeList && pathname.includes("/banker") && search) {
        exchangeList.forEach((exchange, i) => {
          if (exchange._id === search) {
            setIndex(i);
          }
        });
      } else if (pathname.includes("/bank") && instituteList && search) {
        instituteList.forEach((institute, i) => {
          if (
            institute?.institute_name
              ?.toLowerCase()
              ?.includes(search?.toLowerCase())
          ) {
            setIndex(i);
          }
        });
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [search]);
  return (
    <>
      <SearchNavbar
        type={pathname.includes("/banker") ? "banker" : "bank"}
        setType={(type) => {
          history.push(`/${type}`);
        }}
        search={search}
        setSearch={setSearch}
      />
      <div className="institutionDetail" ref={ref}>
        {bankerAddStep > 0 ? (
          <BankerDetailSide />
        ) : (
          <div className="carouselWrapper">
            <div
              className="carouselVert"
              {...handlers}
              onWheel={(e) => {
                if (e.deltaY > 0) {
                  if (count > 15) {
                    next();
                    setCount(0);
                  } else {
                    setCount(count + 1);
                  }
                } else if (e.deltaY < 0) {
                  if (count > 15) {
                    prev();
                    setCount(0);
                  } else {
                    setCount(count + 1);
                  }
                }
              }}
            >
              {loading ? (
                <>
                  <div className="bankItem active">
                    <Skeleton className="logo" />
                    <Skeleton className="bankName" width={400} />
                    <Skeleton className="country" />
                  </div>
                  <div className="bankItem next">
                    <Skeleton className="logo" />
                    <Skeleton className="bankName" width={400} />
                    <Skeleton className="country" />
                  </div>
                  <div className="bankItem nextnext">
                    <Skeleton className="logo" />
                    <Skeleton className="bankName" width={400} />
                    <Skeleton className="country" />
                  </div>
                  <div className="bankItem prevprev">
                    <Skeleton className="logo" />
                    <Skeleton className="bankName" width={400} />
                    <Skeleton className="country" />
                  </div>
                  <div className="bankItem prev">
                    <Skeleton className="logo" />
                    <Skeleton className="bankName" width={400} />
                    <Skeleton className="country" />
                  </div>
                </>
              ) : !pathname.includes("/banker") || showInstitutes ? (
                instituteList.map((institute, i) => {
                  return (
                    <div
                      className={`bankItem ${
                        index === i
                          ? "active"
                          : isPrev(i, instituteList)
                          ? "prev"
                          : isNext(i, instituteList)
                          ? "next"
                          : isPrevPrev(i, instituteList)
                          ? "prevprev"
                          : isNextNext(i, instituteList)
                          ? "nextnext"
                          : "inactive"
                      }`}
                      onClick={() => {
                        setInstitutionSelected(institute);
                        setIndex(i);
                      }}
                      key={institute._id}
                    >
                      <img
                        className="logo"
                        src={institute.profile_image}
                        alt=""
                      />
                      <span className="bankName">
                        {institute.institute_name}
                      </span>
                      <div className="country">{institute.country_name}</div>
                    </div>
                  );
                })
              ) : (
                exchangeList.map((banker, i) => (
                  <div
                    className={`bankItem ${
                      index === i
                        ? "active"
                        : isPrev(i, exchangeList)
                        ? "prev"
                        : isNext(i, exchangeList)
                        ? "next"
                        : isPrevPrev(i, exchangeList)
                        ? "prevprev"
                        : isNextNext(i, exchangeList)
                        ? "nextnext"
                        : "inactive"
                    }`}
                    onClick={() => {
                      setBankerSelected(banker);
                      setIndex(i);
                    }}
                    key={banker._id}
                  >
                    <img className="logo" src={banker.profilePicURL} alt="" />
                    <span className="bankName">{banker.bankerTag}</span>
                    <div className="country">{banker.country}</div>
                  </div>
                ))
              )}
            </div>
          </div>
        )}
        <div className="detailSection">
          {getTabContent()}
          <div className="bottomTabs">
            <div
              className={`tabItm ${tab === "Profile"}`}
              onClick={() => setTab("Profile")}
            >
              Profile
            </div>
            <div
              className={`tabItm ${tab === "ATM’s"}`}
              onClick={() => setTab("ATM’s")}
            >
              ATM’s
            </div>
            <div
              className={`tabItm ${tab === "Reviews"}`}
              onClick={() => setTab("Reviews")}
            >
              Reviews
            </div>
            {pathname.includes("/banker") ? (
              <div
                className={`tabItm ${tab === "Manage"}`}
                onClick={() => setTab("Manage")}
              >
                Manage
              </div>
            ) : (
              <div
                className={`tabItm ${tab === "Manages"}`}
                onClick={() => setTab("Manages")}
              >
                Manage
              </div>
            )}
          </div>
        </div>
      </div>
    </>
  );
}

export default InstitutionDetailPage;
