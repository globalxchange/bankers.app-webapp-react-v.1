import React, { useState } from "react";
import { Link } from "react-router-dom";
import Navbar from "../../Components/Layouts/navbar";
import SearchTxnComponent from "../../Components/SearchTxnComponent/SearchTxnComponent";

function SerchTxnPage() {
  const [cbw, setCbw] = useState(false);
  return (
    <>
      <Navbar invert className="searchNav" />
      <div className="serchTxnPage">
        <SearchTxnComponent cbw={cbw} setCbw={setCbw} />
        <div className={`resArea ${!cbw && "hide"}`}>
          <div className="title">
            <span className="mainText">Don’t Have A Code?</span>
            <span className="mobText">Find The Best</span>
          </div>
          <div className="subTitle">
            Click On The Type Of Data You Want To See
          </div>
          <div className="optButtons">
            <Link to="/institution" className="optBtn">
              Institutions
            </Link>
            <Link to="/exchanges" className="optBtn">
              Exchanges
            </Link>
          </div>
          <div className="mobileFooter" onClick={() => setCbw(false)}>
            Go Back To Search
          </div>
        </div>
      </div>
    </>
  );
}

export default SerchTxnPage;
